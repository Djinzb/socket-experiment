export default (req, res) => {
  if (req.method === "POST") {
    const gameId = req.body;

    res?.socket?.server?.io?.emit("startGame", gameId);

    res.status(201).json(gameId);
  }
};
